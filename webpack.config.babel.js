import webpackMerge from 'webpack-merge'
import path from 'path'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import webpack from 'webpack'
import rimraf from 'rimraf'

const baseConfig = {
  entry: {
    app: [
      'react-hot-loader/patch',
      './src/index.js',
    ],
  },
  output: {
    filename: 'js/[name].js',
    path: path.resolve(__dirname, './public'),
    publicPath: '/'
  },
  module: {
    rules: [
      { test: /\.(js|jsx)$/, use: 'babel-loader', exclude: /node_modules/ },
      // { test: /\.(js|jsx)$/, use: 'eslint-loader', exclude: /node_modules/ },
      {
              test: /\.css$/,
              use: [ 'style-loader', 'css-loader', 'font-loader?format[]=truetype&format[]=woff&format[]=embedded-opentype' ]
            },
            {
              test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
              exclude: /(node_modules|bower_components)/,
              loader: "file-loader?limit=10000&mimetype=image/svg+xml&name=[name].[hash].[ext]"
            },
            {
              test: /\.gif/,
              exclude: /(node_modules|bower_components)/,
              loader: "file-loader?limit=10000&mimetype=image/gif&name=[name].[hash].[ext]"
            },
            {
              test: /\.jpg/,
              exclude: /(node_modules|bower_components)/,
              loader: "file-loader?limit=10000&mimetype=image/jpg&name=[name].[hash].[ext]"
            },
            {
              test: /\.ico/,
              exclude: /(node_modules|bower_components)/,
              loader: "file-loader?limit=10000&mimetype=image/ico&name=[name].[hash].[ext]"
            },
            {
              test: /\.png/,
              exclude: /(node_modules|bower_components)/,
              loader: "file-loader?limit=10000&mimetype=image/png&name=[name].[hash].[ext]"
            },
            {
              test: /\.(otf|eot|ttf|woff|woff2)$/,
              loader: 'file-loader?name=assets/fonts/[name].[ext]'
            },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [
      path.resolve('./src'),
      path.resolve('./node_modules'),
    ],
  },
  plugins: [
    {
      apply: compiler => {
        rimraf.sync(compiler.options.output.path);
      }
    },
    new HtmlWebpackPlugin({
      filename: '../public/index.html',
      template: './src/index.html',
    }),
  ],
}

export default (env) => {
  const envConfig = require('./config/' + env + '.js').default

  return webpackMerge(baseConfig, envConfig)
}