import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { renderRoutes } from 'react-router-config'
import createRoutes from './routes'

export default ({ store }) => <BrowserRouter>
  {renderRoutes(createRoutes(store))}
</BrowserRouter>
