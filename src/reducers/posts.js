import { RSAA } from 'redux-api-middleware'
import createRequestAction from 'utils/createRequestAction'
import timestamp from 'utils/timestamp'
import apiUrl from 'constants/apiUrl'

const KEY = 'posts'
const FETCH = createRequestAction(`${KEY}/fetch`)
const CREATE = createRequestAction(`${KEY}/create`)
const UPDATE = createRequestAction(`${KEY}/update`)
const DELETE = createRequestAction(`${KEY}/delete`)

function fetchPosts() {
  return {
    [RSAA]: {
      endpoint: `${apiUrl}/posts`,
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
      credentials: 'include',
      types: [
        FETCH.REQUEST,
        FETCH.SUCCESS,
        FETCH.FAILURE,
      ]
    }
  }
}

const initState = {
  data: [],
  error: null,
  receivedAt: null,
  isLoading: false,
}

const actionHandlers = {
  [FETCH.REQUEST]: (state, action) => ({
    ...state,
    error: null,
    isLoading: true,
  }),
  [FETCH.SUCCESS]: (state, action) => ({
    ...state,
    isLoading: false,
    data: action.payload,
    receivedAt: timestamp(),
  }),
  [FETCH.FAILURE]: (state, action) => ({
    ...state,
    receivedAt: timestamp(),
    error: action.payload,
    isLoading: false,
  }),
}

const actionCreators = {
  fetchPosts,
}
const actionTypes = {
  FETCH,
  CREATE,
  UPDATE,
  DELETE,
}

export {
  actionCreators,
  actionTypes,
  initState,
}

export default (state = initState, action) => {
  const handler = actionHandlers[action.type]

  return handler ? handler(state, action) : state
}