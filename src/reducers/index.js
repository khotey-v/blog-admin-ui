import { reducer as form } from 'redux-form'
import auth from './auth'
import posts from './posts'

export default {
  form,
  auth,
  posts,
}
