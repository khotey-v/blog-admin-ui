import { RSAA } from 'redux-api-middleware'
import cookie from 'js-cookie'
import createRequestAction from 'utils/createRequestAction'
import timestamp from 'utils/timestamp'
import apiUrl from 'constants/apiUrl'

const KEY = 'auth'
const SIGN_IN = createRequestAction(`${KEY}/sign-in`)

const initState = {
  isLogged: localStorage.isLogged || false,
  entities: {
    user: {
      error: null,
      receivedAt: null,
      data: {},
    },
    signIn: {
      error: null,
      receivedAt: null,
      isLoading: false,
    }
  }
};

function signIn(userData) {
  return {
    [RSAA]: {
      endpoint: `${apiUrl}/sign-in`,
      method: 'POST',
      body: JSON.stringify(userData),
      headers: { 'Content-Type': 'application/json' },
      types: [
        SIGN_IN.REQUEST,
        SIGN_IN.SUCCESS,
        SIGN_IN.FAILURE,
      ]
    }
  }
}

const actionHandlers = {
  [SIGN_IN.REQUEST]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      signIn: {
        ...state.signIn,
        isLoading: true,
        error: null,
      }
    }
  }),
  [SIGN_IN.SUCCESS]: (state, action) => {
    localStorage.setItem('isLogged', true)
    cookie.set('XSRF-TOKEN', action.payload, { path: '' })

    return {
      ...state,
      isLogged: true,
      entities: {
        ...state.entities,
        signIn: {
          ...state.entities.signIn,
          receivedAt: timestamp(),
          isLoading: false,
        }
      }
    }
  },
  [SIGN_IN.FAILURE]: (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      signIn: {
        ...state.entities.signIn,
        receivedAt: timestamp(),
        error: action.payload,
        isLoading: false,
      }
    }
  }),
}

const actionCreators = {
  signIn,
}

const actionTypes = {
  SIGN_IN,
}

export {
  actionTypes,
  actionCreators,
  initState,
}

export default (state = initState, action) => {
  const handler = actionHandlers[action.type]

  return handler ? handler(state, action) : state
}