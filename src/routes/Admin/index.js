import AdminLayout from './layout/AdminLayout'
import routes from './routes'

export default store => {
  return {
    path: '/',
    component: AdminLayout,
    routes: routes(store),
  }
}