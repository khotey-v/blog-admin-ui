import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { renderRoutes } from 'react-router-config'
import nav from 'constants/nav'

class AdminLayout extends Component {
  componentWillMount() {
    const { auth: { isLogged } } = this.props;

    if (!isLogged) {
      this.props.history.push('/sign-in')
    }
  }

  render() {
    const { route } = this.props

    return <div className="row">
      <div className="col-xs-12 col-sm-3">
        <div className="list-group">
          {
            nav.map(nav => <Link
              className="list-group-item list-group-item-action"
              key={nav.url}
              to={nav.url}
              >
              {nav.label}
            </Link>)
          }
        </div>
      </div>
      <div className="col-xs-12 col-sm-9">
        {renderRoutes(route.routes)}
      </div>
    </div>
  }
}

function mstp({ auth }) {
  return { auth }
}

export default connect(mstp)(AdminLayout)