import React from 'react'
import PostsRoutes from './Posts'

export default store => {
  return [
    PostsRoutes(store),
  ]
}