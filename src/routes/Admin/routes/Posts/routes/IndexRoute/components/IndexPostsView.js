import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class IndexPostsView extends Component {
  componentDidMount() {
    this.props.fetchPosts()
  }

  renderPosts() {
    const { posts: { data, receivedAt } } = this.props

    if (receivedAt && !data.length) {
      return <div>No posts ;(</div>
    }

    return data.map(post => <div>{post.title}</div>)
  }

  render() {
    const { posts: { isLoading, error } } = this.props

    if (isLoading) {
      return <div>Posts Loading...</div>
    }

    if (error) {
      return <div>{error.response.message}</div>
    }

    return <div>
      <nav>
        <Link to="/posts/create" className="btn btn-primary">Create</Link>
      </nav>
      {this.renderPosts()}
    </div>
  }
}