import { connect } from 'react-redux'
import IndexPostsView from '../components/IndexPostsView'
import { actionCreators } from 'reducers/posts'

function mstp({ posts }) {
  return { posts }
}

export default connect(mstp, {
  fetchPosts: actionCreators.fetchPosts
})(IndexPostsView)