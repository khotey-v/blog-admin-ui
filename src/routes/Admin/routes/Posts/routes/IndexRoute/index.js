import IndexPostsContainer from './container/IndexPostsContainer'

export default store => {
  return {
    path: '/posts',
    component: IndexPostsContainer,
    exact: true,
  }
}