import IndexRoute from './IndexRoute'
import CreatePostRoute from './CreatePost'

export default store => {
  return [
    IndexRoute(store),
    CreatePostRoute(store),
  ]
}