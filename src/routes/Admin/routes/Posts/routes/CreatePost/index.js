import CreatePostContainer from './container/CreatePostContainer'

export default store => {
  return {
    path: '/posts/create',
    component: CreatePostContainer
  }
}