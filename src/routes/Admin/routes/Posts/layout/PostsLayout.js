import React from 'react'
import { renderRoutes } from 'react-router-config'

export default ({ route }) => <div>
  <h2>Posts</h2>
  {renderRoutes(route.routes)}
</div>