import PostsLayout from './layout/PostsLayout'
import routes from './routes'

export default store => {
  return {
    path: '/posts',
    component: PostsLayout,
    routes: routes(store)
  }
}