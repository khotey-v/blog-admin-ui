import React from 'react'
import LoginRoute from './Login'
import Layout from './Layout'
import AdminRoute from './Admin'

function createRoutes(store) {
  return [{
    component: Layout,
    routes: [
      LoginRoute(store),
      AdminRoute(store),
    ]
  }]
}

export default createRoutes
