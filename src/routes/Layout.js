import React, { Component } from 'react'
import { renderRoutes } from 'react-router-config'

export default class Layout extends Component {
  render() {
    const { route } = this.props;

    return renderRoutes(route.routes)
  }
}
