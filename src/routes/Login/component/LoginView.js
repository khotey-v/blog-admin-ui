import React, { Component} from 'react'
import { Field, reduxForm } from 'redux-form'
import { withRouter } from 'react-router-dom'
import './login.css'

class LoginView extends Component {
  componentWillMount() {
    if (this.props.isLogged) {
      this.props.history.push('/')
    }
  }

  onSubmit = async userData => {
    const action = await this.props.signIn(userData)

    if (action && !action.error) {
      this.props.history.push('/')
    }
  };

  render() {
    const { handleSubmit, signInData: { error } } = this.props;

    return (
      <div className="login-container">
        <div className="login">
          <h1>Login</h1>
            <form onSubmit={handleSubmit(this.onSubmit)}>
              {
                error && <div className="alert alert-danger" role="alert">
                  {error.response.message}
                </div>
              }
              <Field component="input" type="text" name="login" placeholder="Username" required="required" />
              <Field component="input" type="password" name="password" placeholder="Password" required="required" />
              <button type="submit" className="btn btn-primary btn-block btn-large">Sign In</button>
            </form>
        </div>
      </div>
    )
  }
}

export default withRouter(reduxForm({ form: 'login' })(LoginView))