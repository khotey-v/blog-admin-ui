import LoginContainer from './container/LoginContainer'

export default store => {
  return {
    path: '/sign-in',
    component: LoginContainer,
  }
}