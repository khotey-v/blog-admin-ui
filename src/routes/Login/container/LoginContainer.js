import { connect } from 'react-redux'
import LoginView from '../component/LoginView'
import { actionCreators } from 'reducers/auth'

function mstp({ auth: { entities: { signIn: signInData }, isLogged } }) {
  return { signInData, isLogged }
}

export default connect(mstp, actionCreators)(LoginView)