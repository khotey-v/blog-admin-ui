import { combineReducers } from 'redux'

import reducers from '../reducers'

export const makeRootReducer = () => combineReducers({
  ...reducers,
})

export default makeRootReducer
