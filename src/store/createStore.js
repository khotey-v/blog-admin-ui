import { applyMiddleware, createStore, compose } from 'redux'
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger'
import { apiMiddleware } from 'redux-api-middleware';
import createReducer from './reducers'

// TODO: create inject reducer function for local reducers
const middlewares = [
  apiMiddleware,
  thunk,
  createLogger(),
]
const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore)

export default (initialState = {}) => createStoreWithMiddleware(createReducer(), initialState)