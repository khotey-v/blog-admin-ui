import { ApiError } from 'redux-api-middleware'

export default store => next => action => {
  if (action) {
    if (action.payload instanceof ApiError) {
      action.payload = JSON.parse(JSON.stringify(action.payload))
    } 
  }

  return next(action)
}