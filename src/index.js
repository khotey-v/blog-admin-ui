import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
/* eslint-disable import/no-extraneous-dependencies */
import { AppContainer } from 'react-hot-loader'
import { Provider } from 'react-redux'

import App from './App'
import store from './store/createStore'

const rootEl = document.querySelector('#root')
const initialStore = store()

const wrapApp = AppComponent => (
  <AppContainer>
    <Provider store={initialStore}>
      <AppComponent store={initialStore} />
    </Provider>
  </AppContainer>
)

render(
  wrapApp(App),
  rootEl,
)

if (module.hot) {
  module.hot.accept('./App', () => {
    /* eslint-disable global-require */
    const NextApp = require('./App').default

    render(wrapApp(NextApp), rootEl)
  })
}
