import webpack from 'webpack'

console.log('%c Running build for development', 'color: #bada55')

export default {
  devtool: 'source-map',
  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('development')
      }
    }),
  ],
  devServer: {
    port: process.env.PORT || 3000,
    contentBase: './public',
    hot: true,
    historyApiFallback: true,
  },
}
